package com.example.edt24;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity {

    ImageView imageView;
    AnimationDrawable animation;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        text = findViewById(R.id.textStart1);
        imageView = findViewById(R.id.imageView);

        animation = new AnimationDrawable();

        animation.addFrame(getResources().getDrawable(R.drawable.man0), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man1), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man2), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man3), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man4), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man5), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man6), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man7), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man8), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man9), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man10), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man11), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man12), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man13), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man14), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man15), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man16), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man17), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man18), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man19), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man20), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man21), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man22), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man23), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man24), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man25), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man26), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man27), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man28), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man29), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man30), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man31), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man32), 20);
        animation.addFrame(getResources().getDrawable(R.drawable.man33), 20);

        animation.setOneShot(false);
        imageView.setImageDrawable(animation);
        animation.start();

        text.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity3.this, MainActivity.class);
            startActivity(i);
        });
    }
}