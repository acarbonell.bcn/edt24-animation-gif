package com.example.edt24;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    ImageView imageView;
    AnimationDrawable animation;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        text = findViewById(R.id.textStart1);
        imageView = findViewById(R.id.imageView);

        animation = new AnimationDrawable();
        String img;
        for (int i = 17; i <= 150; i++){
            img = "rocket";
            animation.addFrame(ResourcesCompat.getDrawable(
                    getResources(), getResources().getIdentifier(
                            img + i, "drawable", getPackageName()),
                    null),
                    30);
        }

        animation.setOneShot(false);
        imageView.setImageDrawable(animation);
        animation.start();

        text.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity2.this, MainActivity3.class);
            startActivity(i);
        });
    }
}